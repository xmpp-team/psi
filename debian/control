Source: psi
Section: net
Priority: optional
Maintainer: Debian XMPP Maintainers <pkg-xmpp-devel@lists.alioth.debian.org>
Uploaders: Jan Niehusmann <jan@debian.org>, Boris Pek <tehnick@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               libaspell-dev,
               libhunspell-dev,
               libidn-dev,
               libminizip-dev,
               libqca-qt5-2-dev,
               libqt5svg5-dev,
               libqt5x11extras5-dev,
               libsm-dev,
               libxss-dev,
               qtbase5-dev,
               qtbase5-dev-tools,
               qtmultimedia5-dev,
               zlib1g-dev
Homepage: https://psi-im.org/
Vcs-Git: https://salsa.debian.org/xmpp-team/psi.git
Vcs-Browser: https://salsa.debian.org/xmpp-team/psi
Standards-Version: 4.5.1
Rules-Requires-Root: no

Package: psi
Architecture: any
Recommends: psi-l10n | psi-translations, sox
Depends: libqca-qt5-2-plugins,
         libqt5sql5-sqlite,
         libsasl2-modules,
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: xdg-utils
Description: Qt-based XMPP client
 Psi is a cross-platform powerful XMPP client designed for experienced users.
 User interface of program is very flexible in customization. For example,
 there are "multi windows" and "all in one" modes, support of different
 iconsets and themes. Psi supports file sharing and audio/video calls. Security
 is also a major consideration, and Psi provides it for both client-to-server
 (TLS) and client-to-client (OpenPGP, OTR, OMEMO) via appropriate plugins.
 .
 Psi+ is a development branch of Psi with rolling release development model.
 Users who wants to receive new features and bug fixes very quickly may use
 Psi+ on daily basis. Users who do not care about new trends and prefer
 constancy may choose Psi as it uses classical development model and its
 releases are quite rare.
 .
 List of supported XEPs you may found at:
 https://github.com/psi-im/psi/wiki/Supported-XEPs
