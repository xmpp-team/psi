Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Psi+
Upstream-Contact: Sergey Ilinykh <rion4ik@gmail.com>
                  Boris Pek <tehnick-8@yandex.ru>
Source: https://sourceforge.net/projects/psi/files/Psi/
 The orig.tar.xz is repackaged upstream tarball with removed prebuilt windows
 binaries (*.exe and *.dll files) [TODO: drop this after Psi-2.0 release]
Files-Excluded: configure.exe
                iris/configure.exe
                src/libpsi/tools/idle/win32/idleui.dll
                win32/tod.exe

Files: *
Copyright: 2003-2020 Psi and Psi+ developers
License: GPL-2+

Files: admin/*
       certs/*
       doc/*
       mac/*
       options/*
       qa/*
       sound/*
       src/*
       tools/*
Copyright: 2001-2008 Justin Karneges <justin@karneges.com>
           2003-2009 Michail Pishchagin <mblsha@users.sourceforge.net>
           2001-2008 Remko Troncon
           2010-2020 Sergey Ilinykh aka Rion <rion4ik@gmail.com>
           2006-2007 Kevin Smith <kevin@kismith.co.uk>
           2006-2008 Martin Hostettler
           2006-2008 Joonas Govenius
           1992-2007 Trolltech ASA.
           2005 SilverSoft.Net (Denis Kozadaev) <denis@silversoft.net>
           2009 Barracuda Networks, Inc.
           2010 Tobias Markmann
           2006 Cestonaro Thilo
           2007 Maciek Niedzielski
           2011 Evgeny Khryukin
           2011 Artem Izmaylov <artem@aimp.ru>
           2010 senu
           2013 Ivan Romanov <drizt@land.ru>
           2017 Vitaly Tonkacheyev aka KukuRuzo
           2020 Boris Pek <tehnick-8@yandex.ru>
License: GPL-2+ with OpenSSL exception
 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation, either version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/GPL-2'.
 .
 As a special exception, Justin Karneges gives permission to link this program
 with the Qt Library (commercial or non-commercial edition), and distribute the
 resulting executable, without including the source code for the Qt library
 in the source distribution.
 .
 As a special exception, Justin Karneges gives permission to link this program
 with the OpenSSL Library (or with modified versions of it that use the same
 license as the "OpenSSL" library), and distribute the linked executables.
 You must obey the GNU General Public License in all respects for all of the
 code used other than "OpenSSL". If you modify this file, you may extend this
 exception to your version of the file, but you are not obligated to do so. If
 you do not wish to do so, delete this exception statement from your version.

Files: iconsets/roster/*
Copyright: 2003-2007 Michail Pishchagin <mblsha@users.sourceforge.net>
           2005-2007 Remko Tronçon (https://el-tramo.be)
           2005-2007 Everaldo Coelho (https://www.everaldo.com)
           2003 Jason Kim <jmkim@uci.edu>
License: LGPL-3

Files: src/psimedia/*
       src/tools/advwidget/*
       src/tools/iconset/*
       src/widgets/*
Copyright: 2008-2009 Barracuda Networks, Inc.
           2003-2007 Michail Pishchagin <mblsha@users.sourceforge.net>
           2003-2007 Justin Karneges <justin@karneges.com>
           2007 Kevin Smith <kevin@kismith.co.uk>
           2009 Sergey Ilinykh aka Rion
           2009 Maciej Niedzielski
           1992-2006 Trolltech AS.
           2010 Evgeny Khryukin
           2010 Vitaly Tonkacheyev aka KukuRuzo
License: LGPL-2.1+

Files: src/tools/crash/crash_sigsegv.*
Copyright: 2003-2007 Justin Karneges <justin@karneges.com>
           2003 Juan F. Codagnone <juam@users.sourceforge.net>
License: BSD-2-clause

Files: src/tools/tunecontroller/plugins/winamp/third-party/wa_ipc.h
Copyright: 1997-2008 Nullsoft, Inc.
License: zlib-License

Files: *.appdata.xml
Copyright: not-applicable
License: cc0-1.0
 No license required for any purpose; the work is not subject to copyright
 in any jurisdiction.

Files: src/plugins/deprecated/noughtsandcrosses/tictac.*
Copyright: 1992-2000 Trolltech AS.
License: public-domain
 This files are part of an example program for Qt. This example program may be
 used, distributed and modified without limitation.

Files: iris/src/xmpp/*
Copyright: 2001-2008 Justin Karneges <justin@karneges.com>
           2008 Remko Troncon
License: LGPL-3

Files: iris/tools/icetunnel/*
       iris/tools/nettool/*
       iris/src/irisnet/*
       iris/src/xmpp/xmpp-core/*
       iris/src/xmpp/xmpp-im/*
Copyright: 2008-2010 Barracuda Networks, Inc.
           2001-2008 Justin Karneges <justin@karneges.com>
           2003-2007 Michail Pishchagin <mblsha@users.sourceforge.net>
           2006 Remko Troncon
           2010-2013 Sergey Ilinykh aka Rion <rion4ik@gmail.com>
           2006 Maciek Niedzielski
           2009-2010 Dennis Schridde
           2018 Aleksey Andreev
License: LGPL-2.1+

Files: iris/src/jdns/*
Copyright: 2005-2006 Justin Karneges <justin@karneges.com>
           2005 Jeremie Miller
License: Expat

Files: src/libpsi/*
Copyright: 2003-2009 Michail Pishchagin <mblsha@users.sourceforge.net>
           2006-2008 Maciej Niedzielski
           2003-2006 Justin Karneges <justin@karneges.com>
           2005-2008 James Chaldecott
           2003-2007 Eric Smith
           2006 Cestonaro Thilo
           2007 Remko Troncon
           2009 Caolán McNamara
           2009 Barracuda Networks, Inc.
           2010 Dmitriy.trt
License: GPL-2+

Files: src/libpsi/tools/idle/*
       src/libpsi/tools/zip/zip.*
Copyright: 2001-2008 Justin Karneges <justin@karneges.com>
           2003 Tarkvara Design Inc.
License: LGPL-2.1+

Files: src/libpsi/tools/globalshortcut/NDKeyboardLayout.*
Copyright: 2010 Nathan Day
License: Expat

Files: src/libpsi/tools/zip/minizip/*
Copyright: 1998 Gilles Vollant
License: zlib-License

Files: cmake/modules/*
       iris/cmake/modules/*
Copyright: 2016-2018 Psi+ Project
           2016-2018 Vitaly Tonkacheyev aka KukuRuzo
           2009 Vittorio Giovara <vittorio.giovara@gmail.com>
License: BSD-3-clause

Files: 3rdparty/qhttp/*
Copyright: 2014-2016 Amir Zamani <azadkuh@live.com>
License: Expat

Files: 3rdparty/http-parser/*
Copyright: Igor Sysoev
           Joyent, Inc.
           Fedor Indutny
License: Expat

Files: themes/chatview/moment-with-locales.js
Copyright: 2011-2015 Tim Wood, Iskren Chernev and Moment.js contributors
           2016 JS Foundation and other contributors
License: Expat

Files: debian/*
Copyright: 2001-2020 Jan Niehusmann <jan@debian.org>
           2021 Boris Pek <tehnick-8@yandex.ru>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-3
 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation, either version 3 of the License.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 can be found in `/usr/share/common-licenses/LGPL-3'.

License: LGPL-2.1+
 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation, either version 2.1 of the License, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: zlib-License
 This software is provided 'as-is', without any express or implied warranty.
 In no event will the authors be held liable for any damages arising from the
 use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it freely,
 subject to the following restrictions:
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.
